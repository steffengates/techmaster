#!/usr/bin/env node

const { words } = require('./buzz');
const { rand, contains, endsWith } = require('./utils');

for(let count = 0; count < 20; count++){
    let used = [];
    let buzz = '';
    for(let i = 0; i < (rand(3)+1); i++){
        let word;
        while(true){
            word = words[rand(words.length)];
            if(!contains(used, word)){
                used.push(word);
                break;
            }
        }
    }
    let ender = '';
    while(true){
        word = words[rand(words.length)];
        if(!contains(used, word) && endsWith(word, 'r')){
            used.push(word);
            break;
        }
    }
    console.log(used.join(' '));
}