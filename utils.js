const capitalize = (s) => s.replace(/(?:^|\s)\S/g, (a) => a.toUpperCase() );

const rand = (max) => Math.floor(Math.random() * Math.floor(max));

const contains = (list, item) => {
    for(entry of list){
        if(item === entry){ return true }
    }
    return false;
}

const endsWith = (word, letter) => word[word.length-1] === letter;

module.exports = {
	capitalize,
	rand,
	contains,
	endsWith
}